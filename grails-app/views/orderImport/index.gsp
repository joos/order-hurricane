<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
    <content tag="nav">
    </content>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h3>${ordersImportedCount} order(s) successfully imported/updated</h3>
            <g:if test="${ordersFailedCount}">
                <h3>${ordersImportedCount} order(s) failed to import/update</h3>
            </g:if>

            <div id="controllers" role="navigation">
                <li class="controller">
                    <g:link controller="order">View Orders</g:link>
                </li>
                <li class="controller">
                    <g:link controller="product">View Products</g:link>
                </li>
                <li class="controller">
                    <g:link controller="customer">View Customers</g:link>
                </li>
            </div>
        </section>
    </div>

</body>
</html>
