package order.hurricane

import org.grails.web.json.JSONObject

/**
 * Created by joos on 2/9/17.
 */
interface DomainFactoryService {
    def createInstance(JSONObject json)
}