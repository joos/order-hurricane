package order.hurricane

import org.springframework.http.HttpStatus
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import java.security.SignatureException
import java.util.regex.Matcher
import java.util.regex.Pattern

class ShopifyAuthenticationService implements AuthenticationService {

    def authenticationConfig

    final Pattern validHostname = Pattern.compile('^[a-z0-9-.]*\\.myshopify\\.com$')

    def restBuilder

    String accessToken

    @Override
    boolean isAuthenticated() {
        return accessToken != null
    }

    @Override
    String getAuthenticationUrl() {
        return authenticationConfig.authenticationUrl
    }

    @Override
    boolean isAuthenticationValid(Map<String, String> params) {
        if(isValidateState(params.state) && isValidHmac(params) && isValidHostname(params.shop)) {
            return requestAccessToken(null, params)
        }
    }

    @Override
    String getAccessTokenHeaderName() {
        return authenticationConfig.accessTokenHeaderName
    }

    protected boolean isValidateState(String state) {
        return state && state.equals(authenticationConfig.state)
    }

    protected boolean isValidHostname(String hostname) {
        Matcher matchHost = validHostname.matcher(hostname)
        return matchHost.find()
    }

    protected boolean isValidHmac(Map<String, String> params) {
        String sentHmac = params.hmac

        //Todo: find a way to ignore the grails params
        params.remove('hmac')
        params.remove('controller')
        params.remove('action')
        params.remove('format')

        params.sort()

        String output = params.collect { k,v -> k.replace('&', '%26').replace('%', '%25').replace('=', '%3D') + '=' + v.replace('&', '%26').replace('%', '%25') }.join('&')

        String generatedHmac = hmac(output, authenticationConfig.clientSecret)

        return sentHmac == generatedHmac
    }

    protected String hmac(String data, String key) throws SignatureException {
        String result
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes('UTF-8'), 'HmacSHA256')

            Mac mac = Mac.getInstance('HmacSHA256')
            mac.init(secretKeySpec)

            byte[] digest = mac.doFinal(data.getBytes('UTF-8'))
            result = digest.encodeHex()
        }
        catch (Exception e) {
            throw new SignatureException('Failed to generate HMAC : ' + e.getMessage());
        }
        return result
    }

    protected boolean requestAccessToken(def config, Map<String, String> params) {
        boolean success = false

        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>()
        form.add('client_id', authenticationConfig.clientId)
        form.add('client_secret', authenticationConfig.clientSecret)
        form.add('code', params.code)

        def resp = restBuilder.post(authenticationConfig.accessTokenRequestUrl) {
            accept('application/json')
            contentType('application/x-www-form-urlencoded')
            body(form)
        }

        if(resp.getStatus() == HttpStatus.OK.value()) {
            accessToken = resp.json.access_token
            success = true
        }

        return success
    }
}
