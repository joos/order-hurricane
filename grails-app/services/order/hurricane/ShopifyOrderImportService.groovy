package order.hurricane

import grails.transaction.Transactional
import org.springframework.http.HttpStatus

@Transactional
class ShopifyOrderImportService implements OrderImportService {

    def restBuilder
    def authenticationService
    def orderFactoryService
    def orderImportConfig

    @Override
    def importOrders() {
        def resp = restBuilder.get(orderImportConfig.orderUrl) {
            accept('application/json')
            header(authenticationService.getAccessTokenHeaderName(), authenticationService.accessToken)
        }

        int successfulOrders = 0
        int failedOrders = 0

        if(resp.getStatus() == HttpStatus.OK.value()) {
            for(o in resp.json.orders) {
                def order = orderFactoryService.createInstance(o)
                if(order.validate()) {
                    order.save()
                    successfulOrders++
                } else {
                    failedOrders++
                    order.errors.allErrors.each {
                        log.error(it.toString())
                    }
                }
            }
        }

        [ordersImportedCount: successfulOrders, ordersFailedCount: failedOrders]
    }
}

