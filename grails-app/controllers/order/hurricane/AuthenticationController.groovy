package order.hurricane

import org.springframework.http.HttpStatus

class AuthenticationController {

    def authenticationService

    def index() {
        if(authenticationService.isAuthenticated()) {
            redirect(controller: params.returnController, action: params.returnAction)
        } else {
            session['returnController'] = params.returnController
            session['returnAction'] = params.returnAction
            redirect(url: authenticationService.getAuthenticationUrl())
        }
    }

    def authenticate() {
        if(authenticationService.isAuthenticated() || authenticationService.isAuthenticationValid(params)) {
            if(session.returnController) {
                redirect(controller: session.returnController, action: session.returnAction)
            } else {
                redirect(uri: '/')
            }
            session.removeAttribute('returnController')
            session.removeAttribute('returnAction')
        } else {
            response.status = HttpStatus.UNAUTHORIZED.value()
        }
    }
}
