package order.hurricane

import org.grails.web.json.JSONObject

/**
 * Created by joos on 2/9/17.
 */
interface CustomerFactoryService {
    def createInstance(JSONObject json)
}