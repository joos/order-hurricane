package order.hurricane

import grails.transaction.Transactional
import org.grails.web.json.JSONObject

@Transactional
class ShopifyCustomerFactoryService implements DomainFactoryService {

    def createInstance(JSONObject customerJson) {
        Customer customer = Customer.findOrCreateById(customerJson.id)

        customer.id = customerJson.id;
        customer.firstName = customerJson.first_name
        customer.lastName = customerJson.last_name

        return customer
    }
}
