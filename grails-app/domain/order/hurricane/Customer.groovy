package order.hurricane

class Customer {

    long id
    String firstName
    String lastName

    static constraints = {
        id generator: 'assigned'
    }
}
