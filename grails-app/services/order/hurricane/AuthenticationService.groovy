package order.hurricane

interface AuthenticationService {
    String accessToken
    boolean isAuthenticated()
    String getAuthenticationUrl()
    boolean isAuthenticationValid(Map<String, String> params)
    String getAccessTokenHeaderName()
}
