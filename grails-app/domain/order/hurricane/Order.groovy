package order.hurricane

class Order {

    long id
    String orderNumber
    BigDecimal subtotalPrice
    BigDecimal totalPrice
    BigDecimal totalTax
    Date createdDate
    long totalWeight
    BigDecimal totalDiscounts
    String contactEmail
    Currency currency
    String financialStatus
    String fulfillmentStatus

    Customer customer
    static hasMany = [orderLines:OrderLine]

    static mapping = {
        table '`order`'
        fulfillmentStatus defaultValue: "'unfulfilled'"
    }

    static constraints = {
        id generator: 'assigned'
        contactEmail email: true, blank: true, nullable: true
        customer nullable: true
    }
}
