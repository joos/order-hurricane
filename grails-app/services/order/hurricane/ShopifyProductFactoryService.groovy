package order.hurricane

import grails.transaction.Transactional
import org.grails.web.json.JSONObject

@Transactional
class ShopifyProductFactoryService implements DomainFactoryService {

    def createInstance(JSONObject productJson) {
        Product product = Product.findOrCreateById(productJson.product_id)

        product.id = productJson.product_id
        product.description = productJson.name
        product.weightInGrams = productJson.grams
        product.sku = productJson.sku

        return product
    }
}
