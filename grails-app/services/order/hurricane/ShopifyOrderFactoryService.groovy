package order.hurricane

import grails.transaction.Transactional
import org.grails.web.json.JSONObject

import javax.xml.bind.DatatypeConverter

@Transactional
class ShopifyOrderFactoryService implements DomainFactoryService {

    def customerFactoryService
    def orderLineFactoryService

    def createInstance(JSONObject orderJson) {
        Order order = Order.findOrCreateById(orderJson.id); //new Order(orderJson)

        order.id = orderJson.id
        order.orderNumber = orderJson.order_number
        order.subtotalPrice = new BigDecimal(orderJson.subtotal_price)
        order.totalPrice = new BigDecimal(orderJson.total_price)
        order.totalTax = new BigDecimal(orderJson.total_tax)
        order.createdDate = DatatypeConverter.parseDateTime(orderJson.created_at).getTime()
        order.totalWeight = orderJson.total_weight
        order.totalDiscounts = new BigDecimal(orderJson.total_discounts)
        order.contactEmail = orderJson.contact_email
        order.currency = Currency.getInstance(orderJson.currency)
        order.financialStatus = orderJson.financial_status
        order.fulfillmentStatus = orderJson.fulfillment_status ?: 'unfulfilled'

        if(orderJson.customer) {
            order.customer = customerFactoryService.createInstance(orderJson.customer);
        }

        order.orderLines = [];
        for(li in orderJson.line_items) {
            order.orderLines.add(orderLineFactoryService.createInstance(li));
        }

        return order
    }
}
