package order.hurricane

class OrderLine {

    long id
    int quantity
    BigDecimal price

    Product product

    static constraints = {
        id generator: 'assigned'
    }
}
