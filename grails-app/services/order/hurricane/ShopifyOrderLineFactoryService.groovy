package order.hurricane

import grails.transaction.Transactional
import org.grails.web.json.JSONObject

@Transactional
class ShopifyOrderLineFactoryService implements DomainFactoryService {

    def productFactoryService

    def createInstance(JSONObject orderLineJson) {
        OrderLine orderLine = OrderLine.findOrCreateById(orderLineJson.id)

        orderLine.id = orderLineJson.id
        orderLine.quantity = orderLineJson.quantity
        orderLine.price = new BigDecimal(orderLineJson.price)

        orderLine.product = productFactoryService.createInstance(orderLineJson);

        return orderLine
    }
}
