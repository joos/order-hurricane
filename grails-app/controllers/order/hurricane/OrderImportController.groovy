package order.hurricane

class OrderImportController {

    def orderImportService
    def authenticationService

    def index() {
        if(authenticationService.isAuthenticated()) {
            respond(orderImportService.importOrders())
        } else {
            redirect(controller: 'authentication', params: [returnController: 'orderImport', returnAction: 'index'])
        }
    }
}
