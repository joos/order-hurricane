import order.hurricane.ShopifyAuthenticationService
import order.hurricane.ShopifyCustomerFactoryService
import order.hurricane.ShopifyOrderFactoryService
import order.hurricane.ShopifyOrderImportService
import order.hurricane.ShopifyOrderLineFactoryService
import order.hurricane.ShopifyProductFactoryService

import grails.plugins.rest.client.RestBuilder

// Place your Spring DSL code here
beans = {
    def config = grailsApplication.config

    orderImportService(ShopifyOrderImportService) {
        restBuilder = ref('restBuilder')
        orderFactoryService = ref('orderFactoryService')
        authenticationService = ref('authenticationService')
        orderImportConfig = ref('orderImportConfig')
    }
    orderFactoryService(ShopifyOrderFactoryService) {
        customerFactoryService = ref('customerFactoryService')
        orderLineFactoryService = ref('orderLineFactoryService')
    }
    orderLineFactoryService(ShopifyOrderLineFactoryService) {
        productFactoryService = ref('productFactoryService')
    }
    customerFactoryService(ShopifyCustomerFactoryService)
    productFactoryService(ShopifyProductFactoryService)
    authenticationService(ShopifyAuthenticationService) {
        authenticationConfig = ref('authenticationConfig')
        restBuilder = ref('restBuilder')
    }
    authenticationConfig(org.springframework.beans.factory.config.MapFactoryBean) {
        sourceMap = config.authentication
    }
    orderImportConfig(org.springframework.beans.factory.config.MapFactoryBean) {
        sourceMap = config.orderImport
    }
    restBuilder(RestBuilder)
}
