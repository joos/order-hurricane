package order.hurricane

class Product {

    long id
    String description
    long weightInGrams
    String sku

    static constraints = {
        id generator: 'assigned'
    }
}
