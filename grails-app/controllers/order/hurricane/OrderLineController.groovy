package order.hurricane

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class OrderLineController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond OrderLine.list(params), model:[orderLineCount: OrderLine.count()]
    }

    def show(OrderLine orderLine) {
        respond orderLine
    }

    def create() {
        respond new OrderLine(params)
    }

    @Transactional
    def save(OrderLine orderLine) {
        if (orderLine == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (orderLine.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond orderLine.errors, view:'create'
            return
        }

        orderLine.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'orderLine.label', default: 'OrderLine'), orderLine.id])
                redirect orderLine
            }
            '*' { respond orderLine, [status: CREATED] }
        }
    }

    def edit(OrderLine orderLine) {
        respond orderLine
    }

    @Transactional
    def update(OrderLine orderLine) {
        if (orderLine == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (orderLine.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond orderLine.errors, view:'edit'
            return
        }

        orderLine.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'orderLine.label', default: 'OrderLine'), orderLine.id])
                redirect orderLine
            }
            '*'{ respond orderLine, [status: OK] }
        }
    }

    @Transactional
    def delete(OrderLine orderLine) {

        if (orderLine == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        orderLine.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'orderLine.label', default: 'OrderLine'), orderLine.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'orderLine.label', default: 'OrderLine'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
